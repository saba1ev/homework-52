import React from 'react'
import './Number.css';


const Numbers = (props) =>{
  return(
    <div>
      <h3 className='Num'>{props.number}</h3>
    </div>
  )
};

export default Numbers;
import React, {Component}from 'react';
import Number from '../Numers/Number';
import './Loto.css';


class Loto extends Component{
  state = {
    numbers: []
  };

  changeNum = () =>{
    let copyState = [...this.state.numbers];
    let numb = [];

    while(numb.length < 5) {
      let number = Math.floor(Math.random() * (33 - 5) + 5);
      if (!numb.includes(number)){
        numb.push(number);
      }
    }

    copyState = numb;
    copyState.sort((a,b) => a -b );

    this.setState({
      numbers: copyState
    });

    console.log(this.state.numbers)

  };
  render() {
    return (
      <div className='Dask'>
        <button className='Button' onClick={this.changeNum}>New numbers</button>
        <div className='FlexNum'>
          {this.state.numbers.map((number, index) => {
            return (
              <Number key={index} number={number}/>
            )
          })}
        </div>
      </div>
    );
  }
}
export default Loto;